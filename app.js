'use strict'

var Q = require('q')

var fs = require('fs')
var stream = fs.createReadStream('data/data-short.csv', { encoding: 'utf8' })
var byline = require('byline')
var lineStream = byline(stream);

var MongoClient = require('mongodb').MongoClient

var redis = require("redis")
var client;

var targetIp = '192.168.0.100'
var columns
var db, collection
var dataCount = 0, mongodbCount = 0, redisCount = 0
var doesFinishReading = false
var before

getMongoDBConnection()
    .then(function (db2) {
        console.log('Getting database connection')
        db = db2
        collection = db.collection('data')
        client = redis.createClient(null, targetIp)
    })
    .then(function () {
        console.log('Cleaning database');
        return Q.all([
            cleanMongoDb(),
            cleanRedis()
        ])
    })
    .then(read)
    .fail(function (error) {
        console.error(error.stack);
    })
    .fin(function () {
//        console.log('Closing database connections');
//        db.close()
//        client.quit()
    })
    .then(function () {
        console.log('Processed ' + dataCount + ' records');
    })
    .done()

function read() {
    console.log('Reading data');
    var deferred = Q.defer()

    lineStream.on('data', getColumns)
    lineStream.on('end', function () {
        doesFinishReading = true
        stream.close()
        deferred.resolve()
    })

    return deferred.promise
}

function cleanMongoDb() {
    var deferred = Q.defer()

    collection.remove({}, function (err, removed) {
        if (err) {
            deferred.reject(err)
        } else {
            deferred.resolve()
        }
    })

    return deferred.promise
}

function cleanRedis() {
    var deferred = Q.defer()

    client.flushdb(function (err, didSucceed) {
        if (err) {
            deferred.reject(err)
        } else {
            deferred.resolve()
        }
    })

    return deferred.promise
}

function insertToMongoDb(obj) {
    var deferred = Q.defer()

    collection.insert(obj, function (error, obj) {
        if (error) {
            console.log(error);
            deferred.reject(error)
        } else {
            ++mongodbCount
            if (doesFinishReading && mongodbCount >= dataCount) {
                var after = process.hrtime()
                var diff = (after[0] * 1e9 + after[1]) - (before[0] * 1e9 + before[1])
                console.log('Insert ' + mongodbCount + ' to MongoDB in average ' + diff / mongodbCount + ' nanoseconds');
                db.close()
                client.quit()
            }
            deferred.resolve(obj)
        }
    })

    return deferred.promise
}

function insertToRedis(obj) {
    var deferred = Q.defer()

    client.set(obj.CaseID, JSON.stringify(obj), function (err, replies) {
        if (err) {
            deferred.reject(err)
        } else {
            ++redisCount
            if (doesFinishReading && redisCount >= dataCount) {
                var after = process.hrtime()
                var diff = (after[0] * 1e9 + after[1]) - (before[0] * 1e9 + before[1])
                console.log('Insert ' + redisCount + ' to Redis in average ' + diff / redisCount + ' nanoseconds');
                client.quit()
                db.close()
            }
            deferred.resolve(replies)
        }
    });

    return deferred.promise
}

function getMongoDBConnection() {
    var deferred = Q.defer()

    MongoClient.connect('mongodb://192.168.0.100:27017/foo', function (error, db) {
        if (error) {
            deferred.reject(error)
            return
        }
        deferred.resolve(db)
    })

    return deferred.promise
}

function getColumns(line) {
    columns = line.split(',')
    lineStream.removeListener('data', getColumns)
    before = process.hrtime()
    lineStream.on('data', getData)
}

function getData(line) {
    var fileds = transform(line)

    var data = {}
    for (var i = 0; i < columns.length; i++) {
        var column = columns[i]
        data[column] = fileds[i]
    }

//    console.log('data: ', data)

    insertToRedis(data)
        .done()
//    insertToMongoDb(data)
//        .done()

    ++dataCount
}

function transform(s) {
    var fields = new Array(columns.length), field
    var fieldIndex = 0
    var start = 0, stringIndex = 0
    var isInQuote = false
    var parsedData

    for (stringIndex = 0; stringIndex < s.length; stringIndex++) {
        var charCode = s.charCodeAt(stringIndex)

        if (!isInQuote && charCode === 44) {  // ','
            field = s.substring(start, stringIndex)
            start = stringIndex + 1

            if (/^\s*[\d.+]+\s*$/g.test(field)) {
                parsedData = parseFloat(field)
                if (isNaN(parsedData)) {
                    console.log('before: ', (field == null) ? '[null]' : field);
                    console.log('after: ', parsedData);
                }
            } else {
                var date = new Date(field)
                if (!isNaN(date)) {
                    parsedData = date
                } else {
                    parsedData = field
                }
            }

            fields[fieldIndex++] = parsedData
        }

        if (charCode === 34) {  // '"'
            isInQuote = !isInQuote
        }
    }

    // Format point
    if (s.length != 0) {
        field = s.substring(start, stringIndex)
        var temp = field.match(/[\d.-]+/g)
        if (temp == null) {
            fields[fieldIndex] = null
        } else {
            parsedData = temp.map(function (data) {
                return parseFloat(data)
            })

            fields[fieldIndex] = {
                latitude: parsedData[0],
                longitude: parsedData[1]
            }
        }
    }

    return fields
}