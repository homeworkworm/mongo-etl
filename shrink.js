'use strict'

var Q = require('q')

var fs = require('fs')
var stream = fs.createReadStream('data/data.csv', { encoding: 'utf8' })
var byline = require('byline')
var lineStream = byline(stream);
var writeStream = fs.createWriteStream('data/data-short.csv', { encoding: 'utf8' })

var max = 30001
var readCount = 0
var writeCount = 0

lineStream.on('data', getData)
lineStream.on('end', function () {
    console.log('Read ' + readCount + ' records');
})

function getData(line) {
    if (++readCount > max) {
        console.log('Closing readStream');
        lineStream.removeListener('data', getData)
        stream.close()
        return
    }

    writeStream.write(line + '\n', function () {
        if (++writeCount === max) {
            console.log('Wrote ' + writeCount + ' records');
            writeStream.close()
        }
    })
}