'use strict'

var Q = require('q')

var MongoClient = require('mongodb').MongoClient,
    format = require('util').format

MongoClient.connect('mongodb://127.0.0.1:27017/foo', function (error, db) {
    if (error) {
        throw error
    }

    var collection = db.collection('foo')
    collection.insert({a: 2}, function (error, docs) {
        console.log('docs:', docs)
        client.set("string key", '{"x": 12}', redis.print);
    })
})

Q.nfcall(foo, 1, 3)
    .then(function (answer) {
        console.log('Answer: ', answer);
    })
    .done()

function foo(x, y, callback) {
    setTimeout(function () {
        if (callback) {
            callback(x + y)
        }
    }, 10)
}